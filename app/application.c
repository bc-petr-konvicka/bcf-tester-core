#include <application.h>

#include <bc_atsha204.h>

#define USB_TALK_DEVICE_ADDRESS "%012llx"

#define GPIO_CHANNEL_NUMBER "p%d"

#define BUTTON "button"

bc_led_t led;

bc_button_t button;

bc_atsha204_t atsha204;

bc_lis2dh12_t lis2dh12;

bc_tmp112_t tmp112_internal;

bc_tmp112_t tmp112;

void button_event_handler(bc_button_t *self, bc_button_event_t event, void *event_param);

void atsha204_event_handler(bc_atsha204_t *self, bc_atsha204_event_t event, void *event_param);

void lis2dh12_event_handler(bc_lis2dh12_t *self, bc_lis2dh12_event_t event, void *event_param);

void tmp112_intenal_event_handler(bc_tmp112_t *self, bc_tmp112_event_t event, void *event_param);

void tmp112_event_handler(bc_tmp112_t *self, bc_tmp112_event_t event, void *event_param);

void spirit1_event_handler(bc_spirit1_event_t event, void *event_param);

void gpio_check_task(void *event_param);

bool gpio_get_active(bc_gpio_channel_t *channel);

void gpio_init(void);

void application_init(void)
{
    bc_log_init(BC_LOG_LEVEL_DEBUG, BC_LOG_TIMESTAMP_OFF);

    bc_log_info("LED ON");
    bc_led_init(&led, BC_GPIO_LED, false, false);
    bc_led_set_mode(&led, BC_LED_MODE_ON);

    bc_button_init(&button, BC_GPIO_BUTTON, 0, 0);
    bc_button_set_event_handler(&button, button_event_handler, BUTTON);

    bc_log_info("INIT ATSHA204");
    bc_atsha204_init(&atsha204, BC_I2C_I2C0, 0x64);
    bc_atsha204_set_event_handler(&atsha204, atsha204_event_handler, "atsha204 " USB_TALK_DEVICE_ADDRESS);

    // TODO Add accel and temp

    bc_log_info("INIT LIS2DH12");
    bc_lis2dh12_init(&lis2dh12, BC_I2C_I2C0, 0x19);
    bc_lis2dh12_set_event_handler(&lis2dh12, lis2dh12_event_handler, "lis2dh12");

    bc_log_info("INIT TMP112");
    bc_tmp112_init(&tmp112_internal, BC_I2C_I2C0, 0x48);
    bc_tmp112_set_event_handler(&tmp112_internal, tmp112_intenal_event_handler, "tmp112_internal");

    bc_log_info("INIT TMP112");
    bc_tmp112_init(&tmp112, BC_I2C_I2C0, 0x48);
    bc_tmp112_set_event_handler(&tmp112, tmp112_event_handler, "tmp112");

    bc_log_info("INIT RADIO");
    bc_spirit1_init();
    bc_spirit1_set_event_handler(spirit1_event_handler, NULL);

    bc_scheduler_register(gpio_check_task, "gpio pin " GPIO_CHANNEL_NUMBER, 1000);
}

void application_task(void)
{
	static int i = 0;

    bc_atsha204_read_serial_number(&atsha204);

    bc_lis2dh12_measure(&lis2dh12);

    bc_tmp112_measure(&tmp112_internal);

    bc_tmp112_measure(&tmp112);

    if (i++ % 3 == 0)
    {
        uint8_t *buffer = bc_spirit1_get_tx_buffer();

        memset(buffer, 0, BC_SPIRIT1_MAX_PACKET_SIZE);

        bc_spirit1_set_tx_length(BC_SPIRIT1_MAX_PACKET_SIZE);

        bc_spirit1_tx();
    }

    bc_scheduler_plan_current_relative(2000);
}

void button_event_handler(bc_button_t *self, bc_button_event_t event, void *event_param)
{
    char *text = (char *) event_param;

    if (event == BC_BUTTON_EVENT_CLICK)
    {
        bc_log_info(text);
    }
}

void atsha204_event_handler(bc_atsha204_t *self, bc_atsha204_event_t event, void *event_param)
{
    char *text = (char *) event_param;

    uint64_t serial_number;

    if (event == BC_ATSHA204_EVENT_SERIAL_NUMBER)
    {
        bc_atsha204_get_serial_number(self, &serial_number, sizeof(serial_number));

        bc_log_info(text, serial_number);
    }
    else if (event == BC_ATSHA204_EVENT_ERROR)
    {
        bc_log_error(text, 0);
    }
}

void lis2dh12_event_handler(bc_lis2dh12_t *self, bc_lis2dh12_event_t event, void *event_param)
{
    char *text = (char *) event_param;

	if (event == BC_LIS2DH12_EVENT_UPDATE)
	{
        bc_log_info(text);
	}
}

void tmp112_intenal_event_handler(bc_tmp112_t *self, bc_tmp112_event_t event, void *event_param)
{
    char *text = (char *) event_param;

	if (event == BC_TMP112_EVENT_UPDATE)
	{
        bc_log_info(text);
	}
}

void tmp112_event_handler(bc_tmp112_t *self, bc_tmp112_event_t event, void *event_param)
{
    char *text = (char *) event_param;

	if (event == BC_TMP112_EVENT_UPDATE)
	{
        bc_log_info(text);
	}
}

void spirit1_event_handler(bc_spirit1_event_t event, void *event_param)
{
    (void) event_param;

    if (event == BC_SPIRIT1_EVENT_RX_DONE)
    {
        size_t length = bc_spirit1_get_rx_length();

        if (length > 0)
        {
            bc_log_info("spirit1 rx ok");
        }
    }
    else if (event == BC_SPIRIT1_EVENT_TX_DONE)
    {
        bc_log_info("spirit1 tx ok");

        bc_spirit1_set_rx_timeout(BC_TICK_INFINITY);

        bc_spirit1_rx();
    }
}

void gpio_check_task(void *event_param)
{
    char *text = (char *) event_param;

	bc_gpio_channel_t channel;

	if (gpio_get_active(&channel))
	{
		if (channel != BC_GPIO_P17 + 1)
		{
			bc_log_info(text, channel);
		}
	}
	else
	{
        bc_log_error("gpio pin error");
	}

    bc_scheduler_plan_current_relative(20);
}

bool gpio_get_active(bc_gpio_channel_t *channel)
{
	*channel = BC_GPIO_P17 + 1;

	for (bc_gpio_channel_t ch = BC_GPIO_P0; ch <= BC_GPIO_P17; ch++)
	{
		if ((ch != BC_GPIO_P10) || (ch != BC_GPIO_P11))
		{
			if (bc_gpio_get_input(ch))
			{
				if (*channel == BC_GPIO_P17 + 1)
				{
					*channel = ch;
				}
				else
				{
					return false;
				}
			}
		}
	}

	return true;
}

void gpio_init(void)
{
	for (bc_gpio_channel_t ch = BC_GPIO_P0; ch <= BC_GPIO_P17; ch++)
	{
		bc_gpio_init(ch);

		bc_gpio_set_mode(ch, BC_GPIO_MODE_INPUT);
	}
}
